import React, { Component } from 'react';
import openSocket from 'socket.io-client';
import logo from './logo.svg';
import './App.css';
import  Widget from './Widget.js';

class App extends Component {
 constructor(props) {
  super(props);
  this.state = {
    weather: [],
    isLoading: false,
    server:'http://localhost'
  };
  
  const socket = openSocket(this.state.server+':3002');
  socket.on('connect', msg => {
    console.log("Estoy conectado")
    console.log(msg);
  });

  socket.on("updateweather", data=>{
    this.getWeather();
  })
}

render() {
  return (
    <div className="App">

    <h1>WHEATER REPORT</h1>
    <h3>ACIDLAB TEST </h3>
    <div className="wheather-widgets col-lg-10 offset-1 col-xs-12">
    <Widget wheater={this.state.weather||[]}/>
    </div>

    </div>
    );
}

componentDidMount(){
  this.getWeather();
}

getWeather() {

  fetch(this.state.server+':3002/countries')
  .then( response => response.json() )
  .then( data => {
    console.log(data);
    let d = data.data;
    this.setState({ weather:d })
  }).catch(function() {
    console.log("error");
  });
}
}

export default App;
