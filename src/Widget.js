import React ,{ Component } from 'react';
import './App.css'
import moment from 'moment-timezone';
class Widget extends Component{
  constructor(props){
    super(props);
    let wheater = this.props.wheater !== null ? this.props.wheater : wheater;
    console.log(wheater)
  }

  render(){
    if(this.props.wheater.length == 0){
      return (<div className="col-lg-12">Hubo problemas al sincronizar la data</div>)
    }

    return(
      <div className="row">
      {

        this.props.wheater.map((w,k)=>{
          return(
            <div key={k} className="col-lg-3 widget">
              <div className="body">
                <div className="location">{w.id}</div>
                <div className="temp">{w.data.currently.temperature}</div>
                <div className="summary">{w.data.currently.summary}</div>
                <div className="hour">{this.getTimeZone(w.data.currently.time,w.data.timezone)}</div>
              </div>
            </div>
            )
        })
      }
      </div>
      )
  }

  getTimeZone(t,z){
    try{
      let hora = new Date( t * 1000 )
      let time = moment.tz(hora, z).format('h:m:s a');
      return time;
    }catch(e){
      return 0;
    }
  }
}

export default Widget;
